import acm.graphics.GCanvas;
import acm.graphics.GLine;
import acm.graphics.GOval;
import acm.graphics.GRect;
import acm.program.Program;

import java.awt.Color;

public class Picture extends Program
{
    public Picture()
    {
        start();
    }

    public void init()
    {
        GCanvas canvas = new GCanvas();
        add(canvas);

        /*GRect r = new GRect(100, 50);
        canvas.add(r, 50, 50);
        r.setFilled(true);

        Color purple = new Color(145, 39, 185);
        r.setColor(purple);

        GLine l = new GLine(400, 50, 200, 100);
        canvas.add(l);*/

        GOval o1 = new GOval(200, 50, 100, 100);
        o1.setFilled(true);
        canvas.add(o1);

        GOval o2 = new GOval(400, 50, 100, 100);
        o2.setFilled(true);
        canvas.add(o2);

        GRect r1 = new GRect(250, 200, 200, 50);
        r1.setFilled(true);
        canvas.add(r1);
    }

    public static void main(String[] args)
    {
        Picture p = new Picture();
    }
}